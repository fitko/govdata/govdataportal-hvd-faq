# GovDataPortal HVD-FAQ

Auf der Unterseite [Hochwertige Datensätze](https://www.govdata.de/web/guest/hochwertige-datensaetze) auf govdata.de wird der aktuelle Wissensstand zu hochwertigen Datensätzen (HVD) als FAQ hinterlegt. Es kam die Bitte auf, nicht nur das letzte Aktualisierungsdatum zu vermerken, sondern auch die geänderten Stellen hervorzuheben.

So entstand die Idee, den Inhalt der Webseite auf diesem Git zu spiegeln und den Diff als Tool zu nutzen.
