# Häufig gestellte Fragen („FAQs“) zur Durchführungsverordnung zur Festlegung bestimmter hochwertiger Datensätze

V1.1 - Stand 18.3.2024


Auf dieser Seite stellen wir Informationen zur [Durchführungsverordnung (EU) 2023/138 zur Festlegung bestimmter hochwertiger Datensätze und der Modalitäten ihrer Veröffentlichung und Weiterverwendung](https://eur-lex.europa.eu/legal-content/DE/TXT/?uri=CELEX%3A32023R0138) (**„DVO-HVD“**) zur Verfügung. Diese FAQs stellen praxisnahe, unverbindliche Empfehlungen dar (abgestimmt zwischen dem Bundesministerium für Wirtschaft und Klimaschutz, dem Bundesministerium des Inneren und für Heimat und GovData), mit dem Ziel der EU- bzw. deutschlandweit einheitlichen Umsetzung der DVO-HVD.

Eine Zusammenfassung der zugrundeliegenden DVO-HVD von GovData ist als [Blogpost](https://www.govdata.de/web/guest/neues/-/blogs/hochwertige-datensatze) verfügbar.

Anregungen und Fragen nehmen wir gerne entgegen, z. B. per E-Mail an: info@govdata.de.

**Inhaltsverzeichnis:**

- Abschnitt 1: Hintergrund und Regelungsinhalte

  1. Wie fügt sich die Durchführungsverordnung zur Festlegung bestimmter hochwertiger Datensätze in die Datengesetzgebung der EU ein?
  2. Was ist die Rechtsnatur der Durchführungsverordnung zur Festlegung bestimmter hochwertiger Datensätze und was regelt sie?
  3. Müssen Daten aufgrund der DVO-HVD neu erhoben werden?
  4. An wen richtet sich die DVO-HVD?
  5. Müssen alle öffentlichen Stellen hochwertige Datensätze anbieten, auch wenn diese bereits an anderer Stelle verfügbar sind?
  6. Ist es möglich, einen hochwertigen Datensatz als Untergruppe in einem umfangreicheren Datensatz, der nicht nur hochwertige Daten enthält, bereitzustellen oder muss ein hochwertiger Datensatz eigenständig, d. h. getrennt von anderen Daten, bereitgestellt werden?
  7. Sind Länder und Kommunen und sonstige öffentliche Stellen für alle thematischen Kategorien der hochwertigen Datensätze originär zuständig? Kann eine auf Bundesebene zuständige Stelle die Bereitstellungspflichten stellvertretend übernehmen?
  8. Wie finde ich heraus, ob eine öffentliche Stelle hochwertige Datensätze veröffentlichen muss?
  9. Wo finde ich weitere Informationen im Internet?
- Abschnitt 2: Technische Umsetzungsfragen

  10. Wie sollen hochwertige Datensätze in Metadaten gekennzeichnet werden?
  11. Was gilt bei gemeinfrei gewordenen oder von vorn herein nicht schutzfähigen hochwertigen Datensätzen?
  12. Welche Lizenzen dürfen bzw. sollten bei Bereitstellung von hochwertigen Datensätzen eingesetzt werden?
  13. Welche technischen Anforderungen bestehen an den Massen-Download via APIs?
  14. In welchem offenen und maschinenlesbaren Format müssen hochwertige Datensätze bereitgestellt werden?
  15. Wenn lediglich gescannte Dokumente (d. h. nicht-maschinenlesbar) inhaltlich als hochwertige Datensätze gelten, müssen diese Scans dann in ein maschinenlesbares Format konvertiert werden?
  16. Welches Tool zur API-Dokumentation ist vorgeschrieben?
  17. In welcher Sprache soll die API-Dokumentation erfolgen?
  18. Ist die in Artikel 3 Abs. 5 DVO-HVD vorgeschriebene Kennzeichnung von hochwertigen Datensätzen in den Metadaten nur für die Berichterstattung der Mitgliedstaaten an die Europäische Kommission relevant?
  19. Wie kann ein DCAT-AP-konformer Metadatensatz erstellt werden?
  - 19.1. (DCAT-AP-konformer Metadatensatz) Allgemein
  - 19.2. (DCAT-AP-konformer Metadatensatz) Relevante Standards und technische Grundlagen
  - 19.3. (DCAT-AP-konformer Metadatensatz) DCAT-AP-konforme Kennzeichnung von Hochwertigen Datensätzen und Zuordnung zur HVD-Kategorie
  - 19.4. (DCAT-AP-konformer Metadatensatz) Berichterstattung
  - 19.5. (DCAT-AP-konformer Metadatensatz) Beispiel eines HVD-Metadatensatzes 12

- Abschnitt 3: Organisatorische Umsetzungsfragen

  20. Artikel 3 Abs. 1 DVO-HVD besagt, dass öffentliche Stellen eine Kontaktstelle für Fragen und Probleme im Zusammenhang mit der API benennen. Kann dies gebündelt auf Ressort-, Bundes- oder Landesebene erfolgen?
  21. Wie erfolgt die Berichterstattung zur HVD-DVO an die EU?
  22. Wie und wann wird die Europäische Kommission eine technische Art und Weise der Berichterstattung an die EU gemäß Artikel 5 DVO-HVD festlegen?

- Abschnitt 4: Konformität mit/Konkurrenzen von nationalem und EU-Recht

  23. Verstößt die DVO-HVD durch die Verpflichtung von Kommunen gegen Subsidiaritätsregeln?
  24. Gehen die Bereitstellungspflichten der DVO-HVD den datenschutzrechtlichen Regelungen vor?
Abschnitt 5: FAQs zu einzelnen thematischen Kategorien
  25. Georaum
  - 25.1. (Kategorie Georaum) Welche Geodatensätze werden von der DVO-HVD erfasst?
  26. (Kategorie Georaum) Wie ist die Formulierung „Datensätze, die zusammen den gesamten Mitgliedstaat abdecken“ im Einführungstext des Anhangs der DVO-HVD zur Kategorie Georaum zu verstehen?
  - 26.1. (Kategorie Georaum) Aktuelle Daten und Archivdaten werden bei Geodaten unterschiedlichen Regimen unterworfen und von unterschiedlichen Stellen - Vermessungsverwaltung und Archivverwaltung – getrennt behandelt. Hintergrund ist eine entsprechend der Archivgesetze und der Katastergesetze arbeitsteilige Aufgabenwahrnehmung. Müssen mit Blick auf Art. 4 Abs. 2 DVO-HVD bisher als Archivdaten behandelte Daten künftig weiterhin von der Vermessungsverwaltung vorgehalten und angeboten werden?
  27. Erdbeobachtung und Umwelt
  - 27.1. (Kategorie Erdbeobachtung und Umwelt) Was bedeutet "historische Versionen von Datensätzen"?
  28. (Kategorie Erdbeobachtung und Umwelt) Wie ist die Formulierung „Datensätze […], die […] zusammen den gesamten Mitgliedstaat abdecken“ im Einführungstext des Anhangs der DVO-HVD zur Kategorie Erdbeobachtung und Umwelt zu verstehen?
  29. Meteorologie
  30. Statistik
  - 30.1. (Kategorie Statistik) Müssen öffentliche Stellen zusätzliche Datensätze erstellen, um den im Anhang der DVO-HVD angegebenen Aufschlüsselungen entsprechen? Wie ist vorzugehen, wenn die Aufschlüsselungen in einem der als Verweis genannten Rechtsakts anders strukturiert sind?
  - 30.2. (Kategorie Statistik) Wenn ein Datensatz nicht von einer öffentlichen Stelle in Deutschland, sondern bereits von Eurostat veröffentlicht wird, sollten die Daten dann zusätzlich auf nationaler Ebene veröffentlicht werden?
  - 30.3. (Kategorie Statistik) Kann ein einziger Datensatz in zwei verschiedenen Tabellen enthalten sein? Das heißt, kann ein Teil der Daten in einer Tabelle enthalten sein, während sich der andere Teil in einer anderen Tabelle befindet (beide Tabellen würden als HVD-Tabellen gekennzeichnet werden)? Oder ist es erforderlich, dass der gesamte Datensatz in einer einzigen Tabelle enthalten ist?
  31. Unternehmen und Eigentümerschaft von Unternehmen
  - 31.1. (Kategorie Unternehmen und Eigentümerschaft von Unternehmen) Wie können Unternehmensdokumente und -abschlüsse, die in nicht maschinenlesbaren Formaten erstellt wurden, zur Verfügung gestellt werden? Wie ist zu verfahren, wenn die Verarbeitung dieser Daten einen unverhältnismäßig hohen Aufwand erfordert, der über den einfachen Vorgang gemäß Art. 5 Abs. 3 PSI-RL hinausgeht?
  32. Mobilität
  - 32.1. (Kategorie Mobilität) Wie verhält sich die Unterkategorie „Verkehrsnetze“ der DVO-HVD zur INSPIRE-Richtlinie und zur Richtlinie für die Einführung intelligenter Verkehrssysteme im Straßenverkehr?
  33. (Kategorie Mobilität) Wie ist die Formulierung „Datensätze, die […] zusammen den gesamten Mitgliedstaat abdecken“ im Einführungstext des Anhangs der DVO-HVD zur Kategorie Mobilität zu verstehen?

# Abschnitt 1: Hintergrund und Regelungsinhalte

## 1. Wie fügt sich die Durchführungsverordnung zur Festlegung bestimmter hochwertiger Datensätze in die Datengesetzgebung der EU ein?

Die EU verfolgt mit dem Ziel der Verbesserung der Verfügbarkeit und Qualität von Daten mit Blick auf den Binnenmarkt eine umfassende (Neu)Regulierung des Datenregimes im privaten und öffentlichen Sektor. Von Seiten der EU sind hier die aktuell relevantesten Regelungen:

- Die [Richtlinie (EU) 2019/1024 über offene Daten und die Weiterverwendung von Informationen des öffentlichen Sektors](https://eur-lex.europa.eu/legal-content/DE/TXT/?uri=CELEX%3A32019L1024) (nach dem englischen Titel Re-use of **P**ublic **S**ector **I**nformation: „**PSI-RL**“) inkl. der der DVO-HVD bzgl. der Weiterverwendung von bei öffentlichen Stellen vorhandenen, nicht zugangsbeschränkten Daten,
- die [Verordnung (EU) 2022/868 des Europäischen Parlaments und des Rates vom 30. Mai 2022 über europäische Daten-Governance und zur Änderung der Verordnung (EU) 2018/1724](https://eur-lex.europa.eu/legal-content/DE/TXT/?uri=CELEX%3A32022R0868) (Data Governance Act) bzgl. Weiterverwendung von bei öffentlichen Stellen vorhandenen, zugangsbeschränkten Daten und
- die [Verordnung (EU) 2023/2854 des Europäischen Parlaments und des Rates vom 13. Dezember 2023 über harmonisierte Vorschriften für einen fairen Datenzugang und eine faire Datennutzung sowie zur Änderung der Verordnung (EU) 2017/2394 und der Richtlinie (EU) 2020/1828](https://eur-lex.europa.eu/eli/reg/2023/2854/oj?locale=de) („**Data Act**“) bzgl. Nutzung und Zugriff auf in der Wirtschaft vorhandene Daten.

In der [EU-Datenstrategie](https://eur-lex.europa.eu/legal-content/DE/TXT/?uri=COM%3A2020%3A66%3AFIN) vom 19.02.2020, als dem übergeordnetes Konzept, ist die Nutzung von Informationen des öffentlichen Sektors durch Unternehmen (G2B-Datennutzung) ein zentrales Element. Die Bereitstellung von Informationen, die sich im Besitz von Behörden befinden, ist langjähriges Bestreben der EU, seit Einführung der PSI-RL im Jahr 2003. Die zugrunde liegende Idee ist einfach: Daten, die mit Geldern der öffentlichen Hand erzeugt worden sind, sollen der gesamten Gesellschaft zugutekommen.

Die ursprüngliche PSI-RL wurde im Jahr 2019 überarbeitet u. a. auch mit Blick auf den Open Data. In Deutschland wird die PSI-RL unter anderem seit 2021 durch das [Datennutzungsgesetz](https://www.gesetze-im-internet.de/dng/index.html#BJNR294200021BJNE000700000) („**DNG**“) umgesetzt. Die PSI-RL und andere sektorspezifische Rechtsvorschriften sollen dafür sorgen, dass der öffentliche Sektor mehr Daten, die er hervorbringt, auch leicht zur Nutzung zugänglich macht (vgl. auch ErwG 8 PSI-RL). Der grundrechtliche Charakter des Zugangs zu Informationen ist hierbei zentral (vgl. ErwG 5 PSI-RL). Aus Sicht bestimmter Akteure, vor allem KMU, aber auch von Akteuren aus Zivilgesellschaft, Forschung und Wissenschaft sind Datensätze häufig nicht EU-weit unter gleichen Bedingungen verfügbar, solche unvollständigen bzw. stark fragmentierten Datensätze sind – gerade für KMU – ressourcenbedingt nur eingeschränkt nutzbar.

Die PSI-RL und die DVO-HVD treffen in diesem Sinne Regelungen für besonders hochwertige Datensätze und deren Bereitstellung. Die DVO-HVD konkretisiert dabei den Teil der PSI-Richtlinie, in dem es um hochwertige Datensätze („**HVD**“) geht (vgl. Artikel 13 ff. PSI-RL, § 9 DNG). Die Regelungen zielen darauf ab sicherzustellen, dass bestimmte von öffentlichen Stellen bereitgestellte Daten leicht zugänglich sind. Es geht dabei im Kern um die bessere Verfügbarkeit von hochwertigen Daten.

## 2. Was ist die Rechtsnatur der Durchführungsverordnung zur Festlegung bestimmter hochwertiger Datensätze und was regelt sie?

Die DVO-HVD sorgt dafür, dass Nutzerinnen und Nutzer öffentliche Daten mit hohem sozioökonomischem Wert einfach und kostenlos weiterverwenden können (vgl. ErwG 2 DVO-HVD; nach § 10 Abs. 3 Satz 1 DNG können für HVD u. a keine Grenzkosten erhoben werden). Die DVO-HVD erfasst dabei nur solche Datensätze, die öffentliche Stellen (Bund, Länder, Kommunen) bereits erheben und (ggf. auf Anfrage) anbieten. DVO-HVD enthält selbst keine eigenen Zugangsansprüche, sondern regelt nur die Weiterverwendung, vgl. § 1 Abs. 2 DNG (Achtung: Wurde der Zugang zu Daten bereits einmal eingeräumt, so besteht aufgrund der DVO-HVD ab diesem Zeitpunkt allerdings ein _mittelbarer_ Zugangsanspruch).

Die DVO-HVD geht zurück auf die PSI-RL, die in Deutschland unter anderem durch das Datennutzungsgesetz umgesetzt wurde. Hiernach kann die Europäische Kommission sogenannte Durchführungsrechtsakte erlassen, um thematische Kategorien hochwertiger Datensätze festzulegen (Art. 13 Abs. 2 PSI-RL). Mit der DVO-HVD hat die Europäische Kommission für Daten der sechs Kategorien _Georaum_, _Erdbeobachtung und Umwelt_, _Meteorologie_, _Statistik_, _Unternehmen und Eigentümerschaft von Unternehmen_ und _Mobilität_ besondere (technische) Anforderungen an die Weiterverwendungsmodalitäten festgelegt. FAQs zu den einzelnen Kategorien finden Sie weiter unten.

## 3. Müssen Daten aufgrund der DVO-HVD neu erhoben werden?

Nein. Die DVO-HVD erfasst nur Datensätze, die öffentliche Stellen (Bund, Länder, Kommunen) bereits erheben und die von ihnen „gehalten“ werden (vgl. Art. 1 Abs. 1, Art. 3 Abs. 1 DVO-HVD). Sowohl die PSI-RL als auch das DNG regeln die Weiterverwendung bereits vorhandener Inhalte. Mit der DVO-HVD sind keine neuen Erhebungs- oder Bereitstellungspflichten verbunden. Bereitstellungspflichten werden weiterhin durch die bereichsspezifischen Regelungen begründet (z. B. Informationsfreiheitsgesetze und -satzungen, Geodatenzugangsgesetze, das Personenbeförderungsgesetz, etc.).

## 4. An wen richtet sich die DVO-HVD?

Der Anwendungsbereich der DVO-HVD bezieht sich auf „öffentliche Stellen“ im Sinne der PSI-RL bzw. des DNG. Dazu zählen der Staat, Gebietskörperschaften, Einrichtungen des öffentlichen Rechts oder Verbände, die aus einer oder mehreren dieser Körperschaften (oder Einrichtungen des öffentlichen Rechts) bestehen. „Einrichtungen des öffentlichen Rechts“ sind dabei Rechtspersönlichkeiten, die im Allgemeininteresse liegende Aufgaben nicht-kommerzieller Art erfüllen. Sie werden überwiegend vom Staat, von Gebietskörperschaften oder anderen Stellen des öffentlichen Rechts finanziert, unterliegen deren Aufsicht oder die Mitglieder ihrer Führungsorgane sind mehrheitlich von Staat, Gebietskörperschaften oder anderen Einrichtungen des öffentlichen Rechts ernannt worden. Dementsprechend gilt die DVO-HVD für Bund, Länder und Kommunen – sowie für alle sonstigen Einrichtungen, die mindestens eines dieser Merkmale erfüllen (vgl. dazu insgesamt die Definitionen des Art. 2 PSI-RL bzw. § 3 DNG).

## 5. Müssen alle öffentlichen Stellen hochwertige Datensätze anbieten, auch wenn diese bereits an anderer Stelle verfügbar sind?

Nein. Vom Standpunkt der Nutzerinnen und Nutzer aus betrachtet, ist es nachrangig, wo hochwertige Datensätze zu finden sind. Entscheidend ist, dass der jeweilige Datensatz (einschließlich Metadatenbeschreibung etc.) einfach auffindbar ist und unter DVO-HVD-Bedingungen weiterverwendet werden kann. Die Frage, von welcher Stelle und in welcher Form die Daten veröffentlicht werden, ist zweitrangig. Wenn mehr als eine Stelle dieselben hochwertigen Datensätze anbietet, sollte die Stelle, die diese nicht direkt veröffentlicht, darauf hinweisen, wo die relevanten Datensätze zu finden sind (per Hyperlink).

## 6. Ist es möglich, einen hochwertigen Datensatz als Untergruppe in einem umfangreicheren Datensatz, der nicht nur hochwertige Daten enthält, bereitzustellen oder muss ein hochwertiger Datensatz eigenständig, d. h. getrennt von anderen Daten, bereitgestellt werden?

Ja. Grundsätzlich ist die Bereitstellung eines hochwertigen Datensatzes als Teil eines Gesamt-Datensatzes, der nicht nur aus hochwertigen Datensätzen besteht, möglich. Die DVO HVD verbietet eine solche verschachtelte Bereitstellung nicht. Allerdings sollte zuvor geprüft werden, ob keine unmittelbare Bereitstellung möglich ist bzw. ob gute technische und organisatorische Gründe für diese „verschachtelte“ Bereitstellung vorliegen und die Wiederverwendung des als hochwertiger Datensatz qualifizierten Datensatzes nicht unverhältnismäßig erschwert wird.

## 7. Sind Länder und Kommunen und sonstige öffentliche Stellen für alle thematischen Kategorien der hochwertigen Datensätze originär zuständig? Kann eine auf Bundesebene zuständige Stelle die Bereitstellungspflichten stellvertretend übernehmen?

Beide Fragen sind zu bejahen. Grundsätzlich sind Länder und Kommunen und sonstige öffentliche Stellen für alle thematischen Kategorien der hochwertigen Datensätze zuständig.

Es ist möglich, dass hochwertige Datensätze von zentralen Stellen bereitgestellt werden (beispielsweise aus der Kategorie Statistik alleine vom Statistischen Bundesamt oder aus der Kategorie Meteorologie alleine vom Deutschen Wetterdienst). Dies erfordert, dass

- potentielle Wiederverwender auf die Bereitstellung hingewiesen werden (z. B. durch Verlinkung),
- die Datensätze identisch sind oder der Datensatz der zentralen Stelle alle Daten der Landes- bzw. kommunalen Ebene enthält und
- die Veröffentlichungs- und Weitergabemodalitäten der DVO-HVD durch die zentrale Stelle erfüllt werden.

Allerdings erlischt mit einer solchen zentralen Bereitstellung die Bereitstellungspflicht nach DVO-HVD für die Zukunft nicht oder geht auf die zentrale Stelle über (d. h. für die oben genannten Beispiele: Sollte das Statistische Bundesamt oder der Deutsche Wetterdienst die Bereitstellung der entsprechenden Daten einschränken oder ganz einstellen, muss die betroffene öffentliche Stelle der Landes- kommunalen Ebene bzw. die sonstige öffentliche Stelle die Bereitstellung ihrer jeweiligen hochwertigen Datensätze eigenverantwortlich auf anderem Wege sicherstellen).

## 8. Wie finde ich heraus, ob eine öffentliche Stelle hochwertige Datensätze veröffentlichen muss?

Zentral und naheliegend: **Lesen Sie die DVO-HVD und die PSI-RL bzw. das DNG**. Wenn Sie Daten identifizieren, die thematisch von der DVO-HVD betroffen sein könnten, stellen Sie sich folgende Fragen:

1. Sind Daten einer „**öffentliche Stelle**“ im Sinne von Artikel 2 Abs. 1 der PSI-RL (bzw. § 3 Nr. 1 DNG) betroffen?
2. Werden die Daten von einer der **thematischen Kategorien** der DVO-HVD erfasst?

Wenn die Antwort auf beide Fragen „Ja“ lautet, sollten Sie sich genauer mit dem Thema DVO-HVD beschäftigen. Sie müssen hierzu feststellen, ob Ihre Stelle über Daten verfügen, die nach DVO-HVD-Bedingungen bereitgestellt werden müssen. Die DVO-HVD enthält hierzu eine Eingrenzung der zu veröffentlichen Daten (s. v. a. auch Regelungen im Anhang der DVO-HVD), aber auch Hinweise zur Art und Weise der Veröffentlichung. Zu Klärung können Ihnen folgende Leitfragen dienen:

- Um welche **Daten** geht es und wer ist der **Datenhalter**?

  _Es bietet sich an, eine entsprechende Dateninventur systematisch mit einer Abfrage für alle Daten im betroffenen Organisationsbereich durchzuführen._

  - Bestimmen Sie die Daten, über die Sie verfügen oder von denen Sie glauben, dass sie relevant sind.
  - Welche interne Stelle ist für diese Daten verantwortlich?
  - Wer ist dafür intern der beste fachliche und technische Ansprechpartner?
- **Warum** werden die Daten veröffentlicht und von **wem**?

  _Wenn die zu prüfenden Daten bereits veröffentlicht werden, sollten Sie klären, auf welcher Grundlage dies geschieht und wer die verantwortliche Stelle für die Veröffentlichung ist._

  - Werden die Daten aufgrund eines gesetzlichen Anspruchs auf Zugang, einer Bereitstellungspflicht oder auf sonstige Weise öffentlich/zur ausschließlichen Nutzung bereitgestellt?
  - Müssen bei der Veröffentlichung bestimmte Anforderungen erfüllt werden? (D.h. unabhängig von den Anforderungen der DVO HVD)
  - Welche Daten hat Ihre Stelle bereits veröffentlicht bzw. welche dieser Daten werden durch andere Stellen (ggf. zusammen mit anderen Daten) veröffentlicht?

- Handelt es sich um Daten, die **von der DVO-HVD erfasst** sind?

  _Fallen die Daten in die folgenden, von der DVO-HVD festgelegten Kategorien?_

  - Geodaten, Erdbeobachtung und Umwelt, Meteorologie, Statistik, Unternehmen und Eigentümerschaft von Unternehmen, Mobilität (jeweils zu prüfen in Verbindung mit den Beschreibungen im Anhang der DVO-HVD).

- Besteht eine **Pflicht, die Daten als hochwertige Datenätze nach DVO-HVD** bereitzustellen?

  _Wenn Daten, die in die thematischen Kategorien der DVO-HVD fallen, nicht veröffentlicht sind oder noch nicht unter den Voraussetzungen der DVO-HVD veröffentlicht sind, sollten Sie klären, ob eine Bereitstellung nach DVO-HVD erfolgen sollte._

  - Besteht eine Veröffentlichungspflicht?
  - Werden die Daten auf Anfrage herausgegeben (z. B. aufgrund IFG)?
  - Möchten Sie die Daten freiwillig herausgeben?

- Sind die **Anforderungen der DVO-HVD** eingehalten?

  _Die Anforderungen ergeben sich aus Art. 3 und 4 sowie dem Anhang der DVO-HVD. Werden Daten, die unter die DVO-HVD fallen, bereits veröffentlicht, muss evtl. die Art und Weise der Veröffentlichung angepasst werden._

  - Liegen die Daten in einem offenen und maschinenlesbaren Format vor? D.h. liegt ein Dateiformat wie CSV oder XML (nicht PDF, XLSV) vor?
  - Sind die Daten lizenzfrei (dann mit Hinweis, dass keine Verwendungseinschränkungen) oder unter einer DVO-HVD-konformen offenen Lizenz veröffentlicht? (CC0 oder CC BY bzw. gleichwertigen Alternativen)
  - Können die Daten über eine Anwendungsprogrammierschnittstellen („**API**“) abgerufen werden? Gibt es einen Massen-Download? (erforderlich je nach den Anforderungen der DVO HVD)
  - Ist eine Kontaktperson für mit der API zusammenhängende, technische Fragen erkennbar?
  - Gibt es eine HVD-Kennzeichnung in Metadaten?

- **Sicherheitscheck**

  - Bestehen Rechte Dritter an den Daten oder greifen andere Besonderheiten – sind z. B. Betriebsgeheimnisse von Beteiligten betroffen.
  - Stellen Sie sicher, dass keine personenbezogenen Daten enthalten sind.
  - Erheben Sie ein Entgelt für die Daten? Dies ist unter der DVO-HVD grds. nicht möglich.

## 9. Wo finde ich weitere Informationen im Internet?

Eine ausführliche Handreichung (als PDF) und eine Tabelle (als Excel) mit betroffenen Datenthemen und identifizierten Datensätzen stellt _byte - Bayerische Agentur für Digitales GmbH_ für das Land Bayern zur Verfügung: [Link](https://open.bydata.de/info/hvd?locale=de).

# Abschnitt 2: Technische Umsetzungsfragen

## 10. Wie sollen hochwertige Datensätze in Metadaten gekennzeichnet werden?

Hochwertige Datensätze sollen in ihrer Metadatenbeschreibung als solche gekennzeichnet werden (vgl. Art. 3 Abs. 5 DVO-HVD). Die jeweilige Kategorie muss dabei ausgewiesen werden und die Metadaten der hochwertigen Datensätze müssen die für die jeweilige Kategorie angegebenen Eigenschaften umfassen.

Die Eigenschaften können je nach Kategorie auch durch andere jeweils referenzierte Verordnungen bzw. domänenrelevante Standards beschrieben werden (z. B. in der [Richtlinie 2007/2/EG des Europäischen Parlaments und des Rates vom 14. März 2007 zur Schaffung einer Geodateninfrastruktur in der Europäischen Gemeinschaft](https://eur-lex.europa.eu/legal-content/DE/TXT/?uri=celex%3A32007L0002) („**INSPIRE-Richtlinie**“; weitere Informationen dazu [hier](https://eur-lex.europa.eu/DE/legal-content/summary/the-eu-s-infrastructure-for-spatial-information-inspire.html)).

Für eine europäisch einheitliche Metadatenkennzeichnung empfiehlt sich die DCAT-AP-konforme Kennzeichnung (Beispiel hier in den FAQs). GovData begleitet die Entstehung des neues Metadatenstandards DCAT-AP HVD, um eine abgestimmte Auszeichnung in den Metadaten sicherzustellen.

## 11. Was gilt bei gemeinfrei gewordenen oder von vorn herein nicht schutzfähigen hochwertigen Datensätzen?

Hochwertige Datensätze dürfen nur dann unter Nutzungsbedingungen (Lizenzen) bereitgestellt werden, wenn die handelnde öffentliche Stelle über entsprechend lizenzierbare Rechtspositionen verfügt. Es ist dabei unerheblich, ob es sich bei diesen Rechtspositionen um zivilrechtlich begründete Ausschließlichkeitsrechte handelt, etwa um solche gemäß Urheberrechtsgesetz („**UrhG**"), oder um andere Arten von Befugnissen der Zugangskontrolle (so auch Ziffer 2.1 der [Leitlinien für empfohlene Standardlizenzen, Datensätze und Gebühren für die Weiterverwendung von Dokumenten](https://eur-lex.europa.eu/legal-content/DE/TXT/?uri=CELEX%3A52014XC0724%2801%29), auf die sich die DVO-HVD in ihrem ErwG 12 bezieht). Anderenfalls kann es zu einer sogenannten Schutzrechtsberühmung kommen, also einem Suggerieren rechtlicher Kontrollbefugnisse, die in Wahrheit nicht bestehen. Bestehen keine lizenzierbaren Ausschließlichkeitsrechte im zivilrechtlichen Sinne – was nach deutschem Recht etwa bei amtlichen Werken im Sinne des § 5 UrhG der Fall ist – und auch keine sonstigen Befugnisse zur Zugangskontrolle, sind hochwertige Datensätze entsprechend bei der Bereitstellung vorzugsweise mit schlichten Hinweisen darauf zu versehen, dass keine Verwendungseinschränkungen bestehen.

Dabei ist zu beachten, dass das Datenbankherstellerrecht nach § 87b UrhG in diesem Kontext keine lizenzierbare Rechtsposition für öffentliche Stellen begründen kann. Für eine Datenbank im Sinne der RL 96/9/EG (bzw. §§ 87a ff. UrhG) besteht – neben einem etwaigen urheberrechtlichen Schutz für Datenbankwerke nach § 4 Abs. 2 UrhG – zwar nach dem Wortlaut ein Schutzrecht _sui generis_. Dieses gilt aber erstens wohl nicht bei amtlichen Werken gemäß § 5 UrhG (analog). Zweitens verbietet ein solches Vorgehen die PSI-RL selbst; gemäß Art. 1 Abs. 6 PSI-RL (bzw. § 2 Abs. 5 DNG) nehmen öffentliche Stellen ein solches Schutzrecht „_nicht in Anspruch, um dadurch die Weiterverwendung von Dokumenten zu verhindern oder die Weiteverwendung über die in dieser Richtlinie festgelegten Beschränkungen hinaus einzuschränken_“. Diese Regelung lässt sich anhand des Schutzzwecks des § 87b UrhG nachvollziehen, denn Zweck ist hier der Schutz der getätigten Investitionen des Datenbankherstellers durch die Einräumung von z. B. Vervielfältigungs- und Verbreitungsrechten. Die Ausübung dieser entsprechenden Schutzreche, steht aber im Konflikt mit dem Weiterverwendungsgrundsatz der PSI-RL und der DVO-HVD – Daten der öffentlichen Stellen sollen gerade der gesamten Gesellschaft zugutekommen. Sofern in Bezug auf bereitzustellende hochwertige Datensätze also einzig ein Datenbankherstellerrecht nach §§ 87a ff. UrhG als lizenzierbare Rechtsposition in Frage kommt, ist die Verwendung einer Lizenz von Gesetzes wegen nicht zulässig.

Hinweis: Bei der mitunter schwierigen Prüfung, ob lizenzierbare Rechtspositionen bestehen, sollte im Zweifel der Kontakt zu einer fachlich kompetenten Stelle, z. B. der funktional für Open Data zuständigen Stelle, aufgenommen werden.

## 12. Welche Lizenzen dürfen bzw. sollten bei Bereitstellung von hochwertigen Datensätzen eingesetzt werden?

Bestehen lizenzierbare Rechtspositionen, auf die sich die handelnde Stelle auch berufen kann (siehe auch vorige FAQ), sollen öffentliche Stellen gemäß der DVO-HVD hochwertige Datensätze einheitlich unter der „Creative Commons Public Domain Dedication“ (CC-Gemeinfreigabe, CC0) oder der Lizenz „Creative Commons BY 4.0“ (CC-Namensnennung, CC-BY; Informationen [hier](https://creativecommons.org/licenses/by/4.0/)) zugänglich machen (vgl. Artikel 4 sowie ErwG 12 DVO-HVD). Alternativ können hochwertigen Datensätze zwar auch unter einer gleichwertigen oder einer weniger einschränkenden offenen Lizenz angeboten werden. Zu berücksichtigen ist hierbei jedoch, dass die von der DVO-HVD bezweckte Weiterverwendbarkeit (in Deutschland bzw. in der EU) strukturell erschwert wird, je mehr unterschiedliche Lizenzen insgesamt verwendet werden. Die Verwendung einer anderen als den oben genannten CC-Lizenzen ist daher nur dann angezeigt, wenn es dafür im Einzelfall zwingende Gründe gibt. In einem solchen Fall sollte geprüft werden, ob nicht neben dieser abweichenden Lizenz zusätzlich auch unter CC0 bzw. CC-BY 4.0 bereitgestellt werden kann (sogenannte Doppel- oder Parallellizenzierung). Im vorgenannten Sinne abweichende Lizenzen dürfen zudem gemäß DVO-HVD nur bestimmte Bedingungen aufstellen: Zulässig ist zum einen eine Anforderung bezüglich der Namensnennung, wie sie etwa in der Datenlizenz Deutschland - Namensnennung - 2.0 (Informationen [hier](https://www.govdata.de/dl-de/by-2-0)) enthalten ist.

Zum anderen darf von Weiterverwendern verlangt werden, dass sie bereitgestellte Aktualisierungen von Datensätzen bei sich nachvollziehen und den Zeitpunkt solcher Aktualisierungen angeben – sofern dadurch die Weiterverwendung nicht eingeschränkt wird (vgl. ErwG 12 DVO-HVD letzter Satz). Dieses Nachvollziehbarmachen von Änderungen scheint an die Namensnennungsbedingung der CC-Lizenzen angelehnt zu sein. Diese Klausel der CC-Lizenzen erzeugt für Weiterverwender eine ähnliche Pflicht, dies allerdings für den umgekehrten Fall, dass die Weiterverwender aus eigenem Antrieb Änderungen am lizenzierten Material vornehmen. Eine Nach-Aktualisierungspflicht, die durch Aktualisierungen seitens des ursprünglichen Bereitstellers der Daten ausgelöst wird, ist etwas anderes und erfordert eine dauerhafte Überwachung der Aktivitäten dieses Bereitstellers. Der für eine solche Überwachung erforderlichen Ressourceneinsatz schränkt die Weiterverwendung zwangsläufig ein. Die entsprechende Aussage in der DVO-HVD kann daher nur so gemeint sein, dass eine Nach-Aktualisierungspflicht allenfalls im Umfeld der Daten-Bereitstellung verlangt werden kann, nicht aber hinsichtlich der Nutzungsbedingungen der Daten selbst. So verstanden dürfen die eingesetzten Lizenzen keine Nach-Aktualisierungspflichten enthalten, wohl aber etwa Nutzungsbedingungen der APIs, die das Verhältnis zwischen Datenbereitsteller und Datenabfragenden jenseits des Datenmaterials selbst regeln. Weder die oben genannten CC-Lizenzen noch die Datenlizenz Deutschland enthalten eine solche Nach-Aktualisierungspflicht, sind von diesem Aspekt also nicht betroffen.

Hinweis: Damit eine Lizenz mit ihren Bedingungen (bspw. Namensnennung) auch maschinenlesbar ist, muss sie korrekt semantisch ausgezeichnet werden. Für die Lizenz muss eine permanente URI angegeben und die Namensnennung in einem semantisch ausgezeichneten Feld beschrieben werden. In DCAT-AP.de sind entsprechende Felder vorhanden.

Hinweis: Beispiele für andere Varianten der Auszeichnung sind z. B. in einer einfachen Variante [hier](https://blog.llz.uni-halle.de/2013/09/creative-commons-in-maschinenlesbarer-form/) (Blog der Uni Halle), ausführlicher [hier](https://wiki.creativecommons.org/wiki/CC_REL) (Wiki Creative Commons) und [hier](https://chooser-beta.creativecommons.org/) (Creative Commons), wo ein Wizzard mit verschiedenen Ausgabemöglichkeiten beschrieben ist, zu finden.

## 13. Welche technischen Anforderungen bestehen an den Massen-Download via APIs?

Hochwertige Datensätze sollen in maschinenlesbarem Format über APIs und ggf. auch als Massen-Download zur Verfügung gestellt werden (vgl. Art. 3 Abs. 1 DVO-HVD). Bei der Bereitstellung der Lizenzinformation und -bedingungen in maschinenlesbarem Format sollte die Codeliste des Amtes für Veröffentlichungen der EU verwendet werden, siehe bspw. [hier](http://publications.europa.eu/resource/authority/licence/CC_BY_4_0) für die CC BY 4.0 Lizenz. Informationen zur Lizenz sollten mit der Eigenschaft „dct:licence“ (bzw. dcterms:licence) und einem maschinenlesbaren, differenzierbaren URI Wert (dauerhafter Link) und RDF Representation bereitgestellt werden. Soweit eine Lizenz verwendet wird, die mitgliedstaatsspezifisch oder individuell ist, wird empfohlen die Gleichwertigkeit zu CC-BY 4-0 Lizenz maschinenlesbar mit der Eigenschaft „rdfs:seeAlso“ zu kennzeichnen. Sie bezieht sich auf einer gleichwertigen Referenz, die sich auf die Codeliste des EU Veröffentlichungsamtes beschränkt.

Beim Massen-Download handelt es sich um eine Funktion, die das Herunterladen eines vollständigen Datensatzes in einem oder mehreren Paketen ermöglicht. Die API kann beispielsweise durch direkten Zugriff auf Download-Dienste auf der Grundlage der INSPIRE-Richtlinie erfolgen. Die technische Umsetzung erfolgt über die Angabe mindestens einer der folgenden Parameter:

- Access URL (Dcat:accessURL) - URL, die den Zugriff auf eine Verteilung des Datensatzes ermöglicht. Die Ressource unter der Zugriffs-URL kann Informationen darüber enthalten, wie man den Datensatz erhält
- Download URL (Dcat:downloadURL) - URL, die einen direkten Link zu einer herunterladbaren Datei in einem bestimmten Format darstellt.

## 14. In welchem offenen und maschinenlesbaren Format müssen hochwertige Datensätze bereitgestellt werden?

Ein „offenes Format“ ist gem. Artikel 2 Nr. 14 PSI-RL „_ein Dateiformat, das plattformunabhängig ist und der Öffentlichkeit ohne Einschränkungen, die der Weiterverwendung von Dokumenten hinderlich wären, zugänglich gemacht wird._“ Die DVO-HVD gibt hier aber nur allgemeine Regelungen vor, kein bestimmtes Dateiformat für einen Anwendungsfall. Ein „maschinenlesbares Format“ ist gemäß Artikel 2 Nr. 13 PSI-RL „_ein Dateiformat, das so strukturiert ist, dass Softwareanwendungen konkrete Daten, einschließlich einzelner Sachverhaltsdarstellungen und deren interner Struktur, leicht identifizieren, erkennen und extrahieren können_“. Konkrete Formate werden in der DVO-HVD bzw. in deren Anhang näher benannt.

Wir gehen davon aus, dass sich in der Praxis bereits geltende sektorspezifische Regeln durchsetzen werden. So sind z. B. für Daten, die in den Anwendungsbereich der INSPIRE-Richtlinie fallen, die INSPIRE-Richtlinie zu beachten.

Hinweis: Sollten von zentraler Ebene Empfehlungen für ein bestimmtes technisches Format erfolgen, empfiehlt es sich – wenn keine technischen oder sonstigen Gründe dagegensprechen – diesen Empfehlungen zu folgen, um die einheitliche Umsetzung der DVO-HVD zu erreichen.

## 15. Wenn lediglich gescannte Dokumente (d. h. nicht-maschinenlesbar) inhaltlich als hochwertige Datensätze gelten, müssen diese Scans dann in ein maschinenlesbares Format konvertiert werden?

Im Grundsatz: Ja. Artikel 14 Abs. 1 PSI-RL bzw. § 9 DNG und Artikel 3 Abs. 1 DVO-HVD besagen unter anderem, dass hochwertige Datensätzen in einem „maschinenlesbaren Format“ vorliegen müssen. Grundsätzlich sind hochwertige Datensätze demnach in einem maschinenlesbaren Format bereitzustellen. Dies schließt auch den Aufwand einer Konvertierung ein, was sich aus einem Rückschluss aus den in der DVO-HVD erwähnte Ausnahmen (für die Kategorien „Unternehmen und Eigentümerschaft von Unternehmen“ und Erdbeobachtung und Umwelt) ergibt. Die Ausnahmen sind mit Blick auf den Sinn und Zweck der HVD DVO, eine möglichst einfache und umfangreiche Weiterverwendung von Daten zu ermöglichen, eng auszulegen und können daher nicht auf andere Konstellationen übertragen werden.

Für die Kategorie „Unternehmen und Eigentümerschaft von Unternehmen“ gibt es eine Ausnahme von dem Grundsatz der Konvertierungspflicht: "_Maschinenlesbarkeit wird nicht vorgeschrieben für Daten, die in nicht maschinenlesbaren Formaten aufbewahrt werden (z. B. gescannte Unternehmensunterlagen und -abschlüsse) oder für unstrukturierte/nicht maschinenlesbare Datenfelder, die in maschinenlesbaren Dokumenten enthalten sind_ […]“ (s. Ziffer 5.2 lit. a) zweiter Sp. des Anhangs der DVO-HVD). Diese Ausnahme ist der Tatsache geschuldet, dass viele Dokumente, die von Unternehmensregistern aufbewahrt werden, von Unternehmen seit Jahren auf Papier, als Scans oder PDFs abgelegt werden, so dass Unternehmensregister faktisch nicht in der Lage sind, diese Dokumente in maschinenlesbare Formate umzuwandeln. Gleichzeitig handelt es sich um hochwertige Dokumente, deren Wiederverwendbarkeit trotz der Formateinschränkung, gewährleistet werden sollte. Die Bereitstellung von Daten in nicht maschinenlesbarem Format ist in einem solchen Fall möglich.

Eine ähnliche Abschwächung gibt es für historische Daten in der Kategorie „Erdbeobachtung und Umwelt“ in Anhang Ziffer 2.2 lit. a) des Anhangs der DVO-HVD in Bezug auf historische Versionen von Umweltdatensätzen: „_Die Datensätze werden zur Weiterverwendung zur Verfügung gestellt […] über APIs und Massen-Download (für historische Versionen von Datensätzen: APIs oder Massen-Download soweit durchführbar und angemessen)_“.

## 16. Welches Tool zur API-Dokumentation ist vorgeschrieben?

Die DVO-HVD gibt hier nur allgemeine Regelungen vor, kein bestimmtes Tool für eine API-Dokumentation.

## 17. In welcher Sprache soll die API-Dokumentation erfolgen?

Die DVO-HVD enthält hierzu keine Vorgabe. Unsere Empfehlung ist, die Dokumentation auf Deutsch und – wenn möglich – zusätzlich auf Englisch bereitzustellen.

## 18. Ist die in Artikel 3 Abs. 5 DVO-HVD vorgeschriebene Kennzeichnung von hochwertigen Datensätzen in den Metadaten nur für die Berichterstattung der Mitgliedstaaten an die Europäische Kommission relevant?

Nein. Obwohl ein Zusammenhang mit der Berichterstattung an die Europäische Kommission (vgl. Artikel 5 DVO-HVD) nahe liegt, gehen die Ziele von Artikel 3 Abs. 5 DVO-HVD darüber hinaus: Die Auffindbarkeit von hochwertigen Datensätzen ist auch für Weiterverwender von zentraler Wichtigkeit.

Hinweis: Der Standard DCAT-AP-HVD scheint nach aktuellem Stand für die Kennzeichnung nach Artikel 3 Abs. 5 DVO-HVD geeignet zu sein.

## 19. Wie kann ein DCAT-AP-konformer Metadatensatz erstellt werden?

Folgend ist der aktuelle Stand des Diskurses zur Auszeichnung und Beschreibung hochwertigen Datensätzen mittels eines DCAT-AP konformen Metadatensatzes („**HVD-Metadaten**“) zusammengefasst. Dabei beziehen wir uns auf [DCAT-AP HVD](https://semiceu.github.io/DCAT-AP/releases/2.2.0-hvd/), dessen finaler Status „_SEMIC Recommendation_“ ist. Grundsätzlich lässt die DVO-HVD den Weg der Umsetzung offen, es gibt allerdings einen standardisierte Umsetzungsvorschlag durch die [SEMIC](https://joinup.ec.europa.eu/collection/semic-support-centre), den GovData unterstützt und auf den sich folgende Ausführungen beziehen.

### 19.1. (DCAT-AP-konformer Metadatensatz) Allgemein

Die HVD-Metadaten müssen, neben grundsätzlichen Angaben wie bspw. Titel, Beschreibung, Zuständigkeit und Adresse zum Abruf, nur wenige zusätzliche Angaben enthalten. Maßgeblich sollen hochwertige Datensätze als solche gekennzeichnet sein, die jeweilige Kategorie muss gemäß der HVD-DVO ausgewiesen werden und die HVD-Metadaten müssen die für die jeweilige Kategorie angegebenen Eigenschaften umfassen. Letztere können je nach Kategorie auch durch andere jeweils referenzierte Verordnungen bzw. domänenrelevante Standards beschrieben werden (z. B. die INSPIRE-Richtlinie). Die aus der HVD-DVO entstehende Pflicht zur Berichterstattung kann über die [Kataloge der nationalen Datenportale](https://data.europa.eu/data/catalogues), für Deutschland: GovData, erfolgen.

### 19.2. (DCAT-AP-konformer Metadatensatz) Relevante Standards und technische Grundlagen

Die im folgenden beschriebenen Strukturen basieren auf [DCAT3](https://w3c.github.io/dxwg/dcat/), einem Standard des [W3C](https://de.wikipedia.org/wiki/World_Wide_Web_Consortium). GovData erfasst, wie viele europäische Portale, [DCAT-AP](https://semiceu.github.io/DCAT-AP/releases/3.0.0/) konforme Metadaten. Der abgeleitete nationale Standard ist [DCAT-AP.de](https://www.dcat-ap.de/). Im Sinne der DVO-HVD können verschiedene Ressourcen als hochwertige Datensätze ausgewiesen werden, die wichtigsten sind _Datensätze_ und _Datenservices_. Die Anforderungen an Datenservices sind dabei tendenziell umfassender und komplexer, weswegen sich die folgenden Angaben auf die Angaben konzentrieren die alle Ressourcen betreffen.

### 19.3. (DCAT-AP-konformer Metadatensatz) DCAT-AP-konforme Kennzeichnung von Hochwertigen Datensätzen und Zuordnung zur HVD-Kategorie

Eine Bedingung ist, dass hochwertige Datensätze als solche ausgewiesen werden. Dies geschieht mittels der Eigenschaft [dcatap:applicableLegislation](https://semiceu.github.io/uri.semic.eu-generated/r5r/release/3.0.0/#applicableLegislation) die auf eine [dcatap:LegalResource](https://semiceu.github.io/uri.semic.eu-generated/DCAT-AP/releases/2.2.0-hvd/#LegalResource) (bevorzugt [eli:LegalResource](http://data.europa.eu/eli/ontology#LegalResource)) verweisen muss. Im Kontext von hochwertigen Datensätzen muss deren Wert mindestens den ELI Identifikator (http://data.europa.eu/eli/reg_impl/2023/138/oj) der DVO-HVD enthalten. Zusätzlich muss die HVD-Kategorie gemäß des Anhangs der DVO-HVD ausgewiesen werden. Dazu wird [dcatap:hvdCategory](https://semiceu.github.io/uri.semic.eu-generated/r5r/release/3.0.0/#hvdCategory) verwendet, das als Wert den Eintrag der (Haupt-)Kategorie aus [EU Vocabularies HVD Categories](https://op.europa.eu/en/web/eu-vocabularies/dataset/-/resource?uri=http://publications.europa.eu/resource/dataset/high-value-dataset-category) verlangt. Beide Eigenschaften dürfen mehrfach vorkommen (z. B. ein Datensatz erfüllt verschiedene Richtlinien und/oder gehört zu mehreren Kategorien).

### 19.4. (DCAT-AP-konformer Metadatensatz) Berichterstattung

Eine (automatisierte) Bereitstellung eines Katalogs, bestehend aus allen Einträgen der nationalen Datenportale, die als hochwertige Datensätze ausgezeichnet sind, bietet sich als Berichterstattung an. Diesen Katalog kann GovData automatisch bereitstellen. Der Katalog kann mittels eines Profils DCAT-AP HVD auf formale Kriterien hin validiert werden. Neben den genannten Kriterien der Auszeichnung sind das für Datensätze allgemein beispielsweise das Vorhandensein einer Distribution ([dataset distribution](https://semiceu.github.io/uri.semic.eu-generated/DCAT-AP/releases/2.2.0-hvd/#Distribution), dcat:distribution) mit einer Adresse zum Abruf (dcat:accessURL).

### 19.5. (DCAT-AP-konformer Metadatensatz) Beispiel eines HVD-Metadatensatzes

Im Folgenden wird ein Minimalbeispiel für einen validen HVD-Metadatensatz im Format Turtle dargestellt. Zu beachten ist, dass diese Anforderungen nicht die sich aus der jeweiligen Kategorie (z. B. Kategorie Georaum) ergebenden, domänenspezifischen Anforderungen an hochwertige Datensätze erfüllt, sondern nur die abstrakten Anforderungen, die sich zusätzlich aus der Tatsache ergeben, dass es sich um hochwertige Datensätze handelt. Beispielsweise gelten für verschiedene Datensätze, die aus der INSPIRE-Richtlinie hervorgehenden Angabe der Metadatenelemente als _verpflichtend_, die in der [Verordnung (EG) Nr. 1205/2008 der Kommission vom 3. Dezember 2008 zur Durchführung der Richtlinie 2007/2/EG des Europäischen Parlaments und des Rates hinsichtlich Metadaten](https://eur-lex.europa.eu/legal-content/DE/TXT/?uri=CELEX:32008R1205) festgelegt sind.

**Metadatensatz eines Datensatz der eine Bienenpopulation beschreibt**

(Beispiel angelehnt an die [offizielle DCAT-AP Dokumentation](https://semiceu.github.io/DCAT-AP/releases/3.0.0/#example-dataset-series))

_#Anmerkungen sind durch das „#“-Symbol gekennzeichnet. Die beiden **fett** hervorgehobenen Eigenschaften sind dabei HVD spezifisch und gemäß DCAT-AP HVD._

```
prefix : <http://www.example.org/rdf#> #Beispielnamensraum
prefix dcat: <http://www.w3.org/ns/dcat#> #DCAT3 Vokabular
prefix dcatap: <http://data.europa.eu/r5r/> #Namensraum für DCAT-AP definierte Eigenschaften
prefix dct: <http://purl.org/dc/terms/> #Dublin Core Vokabular

#Weist das Objekt http://www.example.org/rdf#/dataset_bienen als Datensatz aus.
:dataset_bienen a dcat:Dataset;
   
   #Titel des Datensatzes
   dct:title "Erhebung der Bienenpopulation"@de;

   #Beschreibung des Datensatzes
   dct:description "Der Datensatz umfasst die Bienenpopulation gemäß einer jährlichen Umfrage von Imker:innen und Landwirt:innen.“@de;

   #HVD DVO zur Bereitstellung von HVD
   dcatap:applicableLegislation <http://data.europa.eu/eli/reg_impl/2023/138/oj>;

   #HVD Kategorie „Erdbeobachtung und Umwelt“
   dcatap:hvdCategory <http://data.europa.eu/bna/c_dd313021>;

   #HVD müssen Distributionen angeben.
   dcat:distribution :distribution_bienen.

#Weist das Objekt als Distribution aus.
:distribution_bienen a dcat:Distribution;

   #Auch für die Distribution muss auf den Rechtsakt verwiesen werden.
   dcatap:applicableLegislation <http://data.europa.eu/eli/reg_impl/2023/138/oj>;

   #URL für direkten Zugriff auf die Distribution (Download)
   dcat:accessURL <http://download.example.org/bulk/distribution_bienen>.
```

# Abschnitt 3: Organisatorische Umsetzungsfragen

## 20. Artikel 3 Abs. 1 DVO-HVD besagt, dass öffentliche Stellen eine Kontaktstelle für Fragen und Probleme im Zusammenhang mit der API benennen. Kann dies gebündelt auf Ressort-, Bundes- oder Landesebene erfolgen?

Nicht ohne weiteres. Die Kontaktstelle nach Art. 3 Abs. 1 DVO-HVD soll die Verfügbarkeit und Pflege der API und letztlich die reibungslose und wirksame Bereitstellung der hochwertigen Datensätze gewährleisten. Hierfür ist eine Sachnähe zum betroffenen Datensatz bzw. der technischen Umgebung erforderlich. Die Einrichtung einer zentralen Kontaktstelle kann damit nur sinnvoll erfolgen, wenn die Kontaktstelle auf die jeweilige technische Umgebung (z. B. mit Administratorenrechten) zugreifen kann. Dies ist mit tiefgreifenden Autorisierungen verbunden, die nach heutigem Stand jedenfalls auf Ressort- oder Bundesebene nicht erfolgen können. Eine zentrale Kontaktstelle auf Ressort- oder Bundesebene für darunter angesiedelte öffentliche Stellen ist daher derzeit nicht möglich.

Allerdings steht GovData als zentrales Informationsportal für Fragen rund um die DVO-HVD zur Verfügung (z. B. per E-Mail an: info@govdata.de).

## 21. Wie erfolgt die Berichterstattung zur HVD-DVO an die EU?

Spätestens zwei Jahre nach Inkrafttreten, d. h. bis zum am 9. Februar 2025, muss Deutschland seiner Berichtspflicht nachkommen und die Europäische Kommission über die Maßnahmen informieren, die von Bund, Ländern und Kommunen zur Umsetzung ergriffen wurden. Gemeldet werden müssen gem. Art. 5 Abs. 3 HVD-DVO:

- eine Liste der hochwertigen Datensätze von Bund, Ländern und Kommunen (mit Online-Verweis auf Metadaten gemäß bestehender Standards, z. B. auf ein zentrales Register oder einen Katalog offener Daten);
- zu jedem hochwertigen Datensatz ein dauerhafter Verweis auf die Lizenzbedingungen der Weiterverwendung;
- zu jedem hochwertigen Datensatz ein dauerhafter Verweis auf die APIs, die den Zugang ermöglichen;
- sofern verfügbar, Hinweise für die Veröffentlichung und Weiterverwendung der hochwertigen Datensätze;
- sowie auf vorhandene Datenschutz-Folgenabschätzungen;
- die Zahl der öffentlichen Stellen, die von der Ausnahmeregel Gebrauch gemacht haben (vgl. Art. 14 Abs 5 PSI-RL bzw. § 10 Abs. 5 DNG).

Derzeit wird mit GovData über technische Möglichkeiten beraten, wie die Meldung der verschiedenen Stellen effizient und aufwandsarm direkt an das Metadatenportal vollzogen werden kann.

## 22. Wie und wann wird die Europäische Kommission eine technische Art und Weise der Berichterstattung an die EU gemäß Artikel 5 DVO-HVD festlegen?

Die DVO-HVD schreibt derzeit keine bestimmte Form für die Berichterstattung vor. Einen Rahmen gibt Artikel 5 Abs. 3 lit. a) bis c) DVO-HVD vor. Im Laufe des Jahres 2024 werden Vorschläge für konkrete technische Lösungen zwischen der Europäischen Kommission und den jeweiligen Mitgliedstaaten ausgetauscht.

Hinweis: Der Standard DCAT-AP-HVD scheint nach aktuellem Stand für die Berichterstattung gemäß Artikel 5 DVO-HVD geeignet zu sein.

# Abschnitt 4: Konformität mit/Konkurrenzen von nationalem und EU-Recht

## 23. Verstößt die DVO-HVD durch die Verpflichtung von Kommunen gegen Subsidiaritätsregeln?

Die europarechtliche Verpflichtung der kommunalen Ebene, bereits zugänglich gemachte hochwertige Daten auch unter bestimmten Weiterverwendungsbedingungen anzubieten, verletzt nicht das Subsidiaritätsprinzip (vgl. [Art. 5 Abs. 3 EUV](https://dejure.org/gesetze/EUV/5.html)). Die Angelegenheit kann nämlich nicht effizienter auf nationaler, regionaler oder lokaler Ebene geregelt werden. Nach einer hierzu durchgeführte Expertenkonsultation und Folgenabschätzung (s [hier](https://ec.europa.eu/info/law/better-regulation/have-your-say/initiatives/12111-Offene-Daten-Verfugbarkeit-offentlicher-Datensatze_de)) steht die DVO-HVD im Einklang mit dem Prüfverfahren (vgl. Art. 5 der [Verordnung (EU) Nr. 182/2011 des Europäischen Parlaments und des Rates vom 16. Februar 2011 zur Festlegung der allgemeinen Regeln und Grundsätze, nach denen die Mitgliedstaaten die Wahrnehmung der Durchführungsbefugnisse durch die Kommission kontrollieren](https://eur-lex.europa.eu/legal-content/DE/ALL/?uri=CELEX%3A32011R0182)) und den Subsidiaritätsanforderungen der PSI-RL. Das Handeln auf europäischer Ebene stellt die Kohärenz des wachsenden EU-Datenmarktes sicher, wirkt nationaler Fragmentierung entgegen und beachtet dabei das Zusammenwirken mit bestehenden Rechtsakten. Bei hochwertigen Datensätzen handelt es sich um Informationen des öffentlichen Sektors, die, wenn sie in offenem und wiederverwendbarem Format zur Verfügung stehen, einen erheblichen Nutzen für Wirtschaft, Gesellschaft und Umwelt in Europa bringen. Da die europäische DVO-HVD nur Datensätze, die bereits bereitgestellt werden, horizontal als hochwertige Datensätze festgelegt, konkretisiert sie bestehende Bereitstellungspflichten. Sie begründet jedoch keine neuen Bereitstellungspflichten. Dies bleibt weiterhin dem nationalen Gesetzgeber überlassen.

## 24. Gehen die Bereitstellungspflichten der DVO-HVD den datenschutzrechtlichen Regelungen vor?

Nein. Vorschriften über den Zugang zu personenbezogenen Daten bleiben von der DVO-HVD unberührt. Sobald die Verarbeitung von hochwertigen Datensätzen personenbezogene Daten betrifft, findet die [Verordnung (EU) 2016/679 des Europäischen Parlaments und des Rates vom 27. April 2016 zum Schutz natürlicher Personen bei der Verarbeitung personenbezogener Daten, zum freien Datenverkehr und zur Aufhebung der Richtlinie 95/46/EG](https://eur-lex.europa.eu/legal-content/DE/TXT/?uri=celex%3A32016R0679) (Datenschutz-Grundverordnung) Anwendung. Wenn hochwertige Datensätze personenbezogene Daten beinhalten, sollen öffentliche Stellen geeignete Techniken (z. B. Generalisierung, Aggregierung, Datenunterdrückung, Anonymisierung, differentielle Privatsphäre oder Randomisierung) anwenden, um so viele Daten wie möglich zur Verfügung stellen zu können (siehe auch ErwG 8 DVO-HVD).

# Abschnitt 5: FAQs zu einzelnen thematischen Kategorien

## 25. Georaum

Aus dem Einführungstext im Anhang der DVO-HVD zur Kategorie Georaum:

Die Kategorie Georaum umfasst Datensätze, die unter die INSPIRE-Datenthemen Verwaltungseinheiten, geografische Bezeichnungen, Adressen, Gebäude und Katasterparzellen gemäß Anhang I und Anhang III der INSPIRE-Richtlinie fallen. Außerdem gehören dazu Referenzparzellen und landwirtschaftliche Parzellen im Sinne der [Verordnung (EU) Nr. 1306/2013 des Europäischen Parlaments und des Rates vom 17. Dezember 2013 über die Finanzierung, die Verwaltung und das Kontrollsystem der Gemeinsamen Agrarpolitik und zur Aufhebung der Verordnungen [...]](https://eur-lex.europa.eu/legal-content/de/TXT/?uri=CELEX%3A32013R1306) und der [Verordnung (EU) Nr. 1307/2013 des Europäischen Parlaments und des Rates vom 17. Dezember 2013 mit Vorschriften über Direktzahlungen an Inhaber landwirtschaftlicher Betriebe im Rahmen von Stützungsregelungen der Gemeinsamen Agrarpolitik und zur Aufhebung der Verordnung (EG) Nr. 637/2008 des Rates und der Verordnung (EG) Nr. 73/2009 des Rates](https://eur-lex.europa.eu/legal-content/DE/TXT/?uri=CELEX%3A32013R1307) sowie der damit verbundenen delegierten Rechtsakte und Durchführungsrechtsakte. Ihre Granularität, geografische Abdeckung und Schlüsselattribute sind [in der Tabelle im Anhang der DVO-HVD] aufgeführt.

### 25.1. (Kategorie Georaum) Welche Geodatensätze werden von der DVO-HVD erfasst?

Grundsätzlich sind Geodatensätze erfasst (Nach Ziffer 2.1 S. 1 des Anhangs der DVO-HVD), die in maschinenlesbarem Format in allen Generalisierungsgraden bis zum Maßstab 1:5.000 angeboten werden, und die (kombiniert) den gesamten Mitgliedstaat abdecken. Wenn Datensätze nur in einer anderen räumlichen Auflösung angeboten werden, müssen öffentliche Stellen (Bund, Länder, Kommunen) diese in der verfügbaren räumlichen Auflösung bereitstellen. Eine Pflicht, Daten in anderen Maßstäben zu erheben, besteht nicht. Nach dem Wortlaut der DVO-HVD muss ein Format (1:5.000, bzw. falls nicht verfügbar, z. B. 1:1.000 oder 1:6.000) kostenlos angeboten werden – darüber hinaus jedoch kein weiteres Format.

## 26. (Kategorie Georaum) Wie ist die Formulierung „Datensätze, die zusammen den gesamten Mitgliedstaat abdecken“ im Einführungstext des Anhangs der DVO-HVD zur Kategorie Georaum zu verstehen?

Hier ist gemeint, dass mehrere ähnlich fragmentierte Datensätze kombiniert das gesamte Bundesgebiet abdecken müssten. Ergibt sich in der Zusammenlegung eine Abdeckung des gesamten Bundesgebiets, ist auch die Bereitstellung eines Teil-Datensatzes erforderlich.

### 26.1. (Kategorie Georaum) Aktuelle Daten und Archivdaten werden bei Geodaten unterschiedlichen Regimen unterworfen und von unterschiedlichen Stellen - Vermessungsverwaltung und Archivverwaltung – getrennt behandelt. Hintergrund ist eine entsprechend der Archivgesetze und der Katastergesetze arbeitsteilige Aufgabenwahrnehmung. Müssen mit Blick auf Art. 4 Abs. 2 DVO-HVD bisher als Archivdaten behandelte Daten künftig weiterhin von der Vermessungsverwaltung vorgehalten und angeboten werden?

Artikel Art. 4 Abs. 2 DVO-HVD lautet: „_Um die Verfügbarkeit von Datensätzen zur Weiterverwendung für längere Zeiträume zu erleichtern, gelten die Verpflichtungen aus dieser Verordnung auch für bestehende maschinenlesbare hochwertige Datensätze, die **vor dem Beginn der Anwendung dieser Verordnung** erstellt wurden_“ (Hervorhebung hinzugefügt).

Der Wortlaut des Art. 4 Abs. 2 DVO-HVD legt in dem in der Frage aufgeworfenen Kontext nahe, dass Datensätze, die bereits bei einer anderen öffentlichen Stelle archiviert worden sind, auch (für die Vergangenheit) bei der erhebenden Stelle vorgehalten werden müssen.

Das ist jedoch nicht erforderlich. Für Datenzugang, Archivierung und Löschung bleiben auch mit Einführung der DVO-HVD die einschlägigen nationalen Geodatengesetze bzw. Archivgesetze des Bundes und der Länder maßgeblich. Die DVO-HVD begründet keine Datenbereitstellungspflicht der Vermessungsverwaltung für Daten, die auf Grundlage des nationalen Geodatengesetze bzw. Archivgesetze des Bundes und der Länder bereits an das Archiv abgegeben wurde und nur noch von diesem bereitgestellt werden.

Einerseits bezieht sich die HVD-DVO nur auf nationale Zugangsansprüche und regelt somit auch nur die Weiterverwendung von Daten, die bereits bereitgestellt werden. Andererseits würde eine Bereitstellungspflicht für Archivdaten durch die Vermessungsverwaltung Ausnahmeregelungen für Archive von der Bereitstellung von hochwertigen Datensätzen unterlaufen (siehe ErwG 6 DVO-HVD, Art. 1 II lit. f) PSI-RL, bzw. Ausnahmen in § 4 II 2, § 10 II Nr. 2 DNG). Die zentrale Aufgabe der Archive, große Datenmengen der Vergangenheit ständig online bereitzuhalten, soll weiterhin gewährleistet bleiben. Der Gesetzgeber hat die Archive bewusst nicht mit einer Pflicht, sondern mit flexiblen rechtlichen Möglichkeiten ausgestattet, um historische Geodaten als Teil einer nationalen Geodateninfrastruktur bereitstellen zu können. Dies würde umgangen werden, wenn stattdessen Vermessungsverwaltungen „archivierte HVD“ bereitstellen müssten.

## 27. Erdbeobachtung und Umwelt

Aus dem Einführungstext im Anhang der DVO-HVD zur Kategorie Erdbeobachtung und Umwelt:

Die Kategorie Erdbeobachtung und Umwelt umfasst Erdbeobachtungsdaten, einschließlich Weltraum- und Fernerkundungsdaten, Boden- oder In-situ-Daten, Umwelt- und Klimadatensätze. Dazu gehören die aktuellsten Datensätze sowie historische Versionen von Datensätzen, die in maschinenlesbarem Format in allen Generalisierungsgraden bis zum Maßstab 1:5.000 vorliegen und zusammen den gesamten Mitgliedstaat abdecken. Wenn Datensätze zwar nicht in diesem Maßstab, aber in höherer räumlicher Auflösung verfügbar sind, so sind sie in der verfügbaren räumlichen Auflösung bereitzustellen.

### 27.1. (Kategorie Erdbeobachtung und Umwelt) Was bedeutet "historische Versionen von Datensätzen"?

„Historische Versionen von Datensätzen“ (vgl. Ziffer 2.1. erster Abs., Ziffer 2.2 lit. a) dritter Sp. des Anhangs zur DVO-HVD) aus der Kategorie „Erdbeobachtung und Umwelt“ sollen veröffentlicht werden, um die Erstellung von Zeitreihen zu ermöglichen. Es handelt sich also um Datensätze, die einen bestimmten abgeschlossenen Zeitraum widerspiegeln und die es dann ermöglichen, die Entwicklung des Wertes über einen längeren Zeitraum zu verfolgen. Handelt es sich bei neueren Versionen der Datensätze lediglich um Korrekturen und Ergänzungen und werden ältere Versionen der aktualisierten Datensätze nicht einmal aufbewahrt, so wird ein solcher älterer Datensatz nach Auffassung der Europäischen Kommission in der Regel nicht als separate "historische Version" des Datensatzes betrachtet. In einem solchen Fall reicht es aus, die aktuell gültige Version zu veröffentlichen, die vollständig ist und die in der Verordnung vorgeschriebene Qualität aufweist. Dies hängt jedoch vom Kontext und den Bedürfnissen der Datennutzer ab. Im Handelsregister beispielsweise sind auch Informationen wichtig, die sich geändert haben (z. B. Rechtsformänderungen, Änderungen des Firmennamens etc.).

## 28. (Kategorie Erdbeobachtung und Umwelt) Wie ist die Formulierung „Datensätze […], die […] zusammen den gesamten Mitgliedstaat abdecken“ im Einführungstext des Anhangs der DVO-HVD zur Kategorie Erdbeobachtung und Umwelt zu verstehen?

Hier ist gemeint, dass mehrere ähnlich fragmentierte Datensätze kombiniert das gesamte Bundesgebiet abdecken müssten. Ergibt sich in der Zusammenlegung eine Abdeckung des gesamten Bundesgebiets, ist auch die Bereitstellung eines Teil-Datensatzes erforderlich.

## 29. Meteorologie

Aus dem Einführungstext im Anhang der DVO-HVD zur Kategorie Meteorologie:

Die thematische Kategorie Meteorologie umfasst Datensätze zu Beobachtungsdaten, die von Wetterstationen gemessen werden, zu validierten Beobachtungen (Klimadaten), Wetterwarnungen, Radardaten und Modelldaten für numerische Wettervorhersagen (NWP) mit der Granularität und den Schlüsselattributen.

## 30. Statistik

Aus dem Einführungstext im Anhang der DVO-HVD zur Kategorie Statistik:

Die thematische Kategorie Statistik umfasst statistische Datensätze mit Ausnahme von Mikrodaten zu den Meldepflichten, die sich aus der in der Verordnung aufgeführten Tabelle mit Rechtsakten ergeben. Zur vollständigen Ermittlung der einschlägigen Rechtsverweise in den Rechtsakten muss bei einigen Datensätzen auf Begriffe aus einer Reihe von Bestimmungen und Anhängen Bezug genommen werden.

### 30.1. (Kategorie Statistik) Müssen öffentliche Stellen zusätzliche Datensätze erstellen, um den im Anhang der DVO-HVD angegebenen Aufschlüsselungen entsprechen? Wie ist vorzugehen, wenn die Aufschlüsselungen in einem der als Verweis genannten Rechtsakts anders strukturiert sind?

Es besteht grundsätzlich keine Pflicht, neue Datensätze zu erheben. Die DVO-HVD gilt lediglich für bereits vorhandene Daten. Der Hauptzweck der Aufschlüsselung im Anhang der DVO-HVD besteht darin, hochwertige Datensätze in den vorhandenen Datensätzen zu identifizieren. Die Europäische Kommission verfolgt zwar auch das Ziel, die Mitgliedstaaten der EU dazu zu bewegen, ihre Dokumente mit dem im Anhang beschriebenen Inhalt und der dort beschriebenen Struktur zu strukturieren, wenn dies noch nicht geschehen sein sollte. Die Vorgaben im Anhang der DVO-HVD sind jedoch häufig aus bestehenden Regelungswerken übernommen, in diesem Fall aus verbindlichen sektoralen Vorschriften für Statistiken.

### 30.2. (Kategorie Statistik) Wenn ein Datensatz nicht von einer öffentlichen Stelle in Deutschland, sondern bereits von Eurostat veröffentlicht wird, sollten die Daten dann zusätzlich auf nationaler Ebene veröffentlicht werden?

Wenn die Datensätze identisch sind, bzw. wenn der Datensatz bei Eurostat alle Daten der nationalen Ebene enthält und die technischen Vorgaben der DVO-HVD durch Eurostat erfüllt werden, ist eine Verlinkung ausreichend. Allerdings erlischt die Bereitstellungspflicht nach DVO-HVD für die Zukunft nicht oder geht auf Eurostat über, d. h. sollte Eurostat die Bereitstellung der entsprechenden Daten einstellen, muss die betroffene öffentliche Stelle die Bereitstellung ihrer jeweiligen hochwertigen Datensätze eigenverantwortlich sicherstellen.

### 30.3. (Kategorie Statistik) Kann ein einziger Datensatz in zwei verschiedenen Tabellen enthalten sein? Das heißt, kann ein Teil der Daten in einer Tabelle enthalten sein, während sich der andere Teil in einer anderen Tabelle befindet (beide Tabellen würden als HVD-Tabellen gekennzeichnet werden)? Oder ist es erforderlich, dass der gesamte Datensatz in einer einzigen Tabelle enthalten ist?

Die DVO-HVD regelt diesen Fall nicht ausdrücklich. Wenn es nachvollziehbare technische und organisatorische Gründe dafür gibt und die Weiterverwendung des eigentlichen hochwertigen Datensatzes nicht erschwert ist, kann ein solches Vorgehen akzeptabel sein.

## 31. Unternehmen und Eigentümerschaft von Unternehmen

Aus dem Einführungstext im Anhang der DVO-HVD zur Kategorie Unternehmen und Eigentümerschaft von Unternehmen:

Die thematische Kategorie Unternehmen und Eigentümerschaft von Unternehmen umfasst Datensätze, die grundlegende Unternehmensinformationen sowie Unternehmensunterlagen und Rechnungsabschlüsse auf der Ebene der Einzelunternehmen mit [näher bestimmten] Schlüsselattribute enthalten.

### 31.1. (Kategorie Unternehmen und Eigentümerschaft von Unternehmen) Wie können Unternehmensdokumente und -abschlüsse, die in nicht maschinenlesbaren Formaten erstellt wurden, zur Verfügung gestellt werden? Wie ist zu verfahren, wenn die Verarbeitung dieser Daten einen unverhältnismäßig hohen Aufwand erfordert, der über den einfachen Vorgang gemäß Art. 5 Abs. 3 PSI-RL hinausgeht?

Art. 5 Abs 3 PSI-RL besagt, dass öffentlichen Stellen aufgrund Art. 5 Abs 1 PSI PSI-RL nicht verpflichtet sind, „_Dokumente neu zu erstellen oder anzupassen oder Auszüge aus Dokumenten zur Verfügung zu stellen, wenn dies mit einem unverhältnismäßigen Aufwand verbunden ist, der über eine einfache Bearbeitung hinausgeht._“

Zwar beruht die DVO-HVD ebenfalls auf der PSI-RL, die Unverhältnismäßigkeitsausnahme des Art. 5 Abs 3 PSI-RL gilt aber für hochwertige Datensätze nicht. Stattdessen ist Artikel 5 Abs. 8 der PSI-RL einschlägig, der die Regelung des Art. 5 Abs. 3 PSI-RL verdrängt.

Allerdings gibt es für die Kategorie „Unternehmen und Eigentümerschaft von Unternehmen“ eine Ausnahme von dem Grundsatz der Konvertierung von Daten in ein maschinenlesbares Format: "_Maschinenlesbarkeit wird nicht vorgeschrieben für Daten, die in nicht maschinenlesbaren Formaten aufbewahrt werden (z. B. gescannte Unternehmensunterlagen und -abschlüsse) oder für unstrukturierte/nicht maschinenlesbare Datenfelder, die in maschinenlesbaren Dokumenten enthalten sind […]_“ (s. Ziffer 5.2 lit. a) zweiter Sp. des Anhangs der DVO-HVD). Die Bereitstellung von Daten in nicht maschinenlesbarem Format ist in einem solchen Fall möglich.

## 32. Mobilität

Aus dem Einführungstext im Anhang der DVO-HVD zur Kategorie Mobilität:

Die thematische Kategorie Mobilität umfasst Datensätze, die unter das INSPIRE-Datenthema Verkehrsnetze gemäß Anhang I der Richtlinie 2007/2/EG fallen und die in allen Generalisierungsgraden bis zum Maßstab 1:5 000 vorliegen und zusammen den gesamten Mitgliedstaat abdecken. Wenn Datensätze zwar nicht im Maßstab 1:5 000, aber in höherer räumlicher Auflösung verfügbar sind, so sind sie in der verfügbaren räumlichen Auflösung bereitzustellen. Die Datensätze enthalten als Schlüsselattribute die nationale Kennnummer, die geografische Position sowie etwaige Verbindungen zu grenzüberschreitenden Netzen.

### 32.1. (Kategorie Mobilität) Wie verhält sich die Unterkategorie „Verkehrsnetze“ der DVO-HVD zur INSPIRE-Richtlinie und zur Richtlinie für die Einführung intelligenter Verkehrssysteme im Straßenverkehr?

Die PSI-RL bzw. die DVO-HVD stellt im Verhältnis zur INSPIRE-Richtlinie und [Richtlinie 2010/40/EU des Europäischen Parlaments und des Rates vom 7. Juli 2010 zum Rahmen für die Einführung intelligenter Verkehrssysteme im Straßenverkehr und für deren Schnittstellen zu anderen Verkehrsträgern](https://eur-lex.europa.eu/legal-content/DE/TXT/?uri=CELEX%3A32010L0040) („**IVS-Richtlinie**“) nur einen Mindeststandard für Weiterverwendungsbedingungen auf. Gleichzeitig berücksichtigt die DVO-HVD die Umsetzung der sektoralen Regelungen der INSPIRE- und IVS-Richtlinie. Verpflichtungen bestimmter privater Unternehmen, ihrerseits Daten bereitzustellen, bleiben auch nach einer Qualifikation als hochwertige Datensätze bestehen. Die PSI-RL setzt die sektoralen Regelungen nicht außer Kraft. So gelten bspw. die Anforderungen der INSPIRE-Richtlinie hinsichtlich der Datenqualität und Interoperabilität fort (vgl. ErwG 9 DVO-HVD). Außerdem sieht die PSI-RL Ausnahmen für die Qualifizierung als hochwertige Datensätze sowie für deren Entgeltlosigkeit (Grundsatz der „Kostenlosigkeit“ in Art. 6 Abs 1 S. 1 PSI-RL) vor (vgl. Art. 14 Abs. 2 UAbs. 2 PSI-RL).

## 33. (Kategorie Mobilität) Wie ist die Formulierung „Datensätze, die […] zusammen den gesamten Mitgliedstaat abdecken“ im Einführungstext des Anhangs der DVO-HVD zur Kategorie Mobilität zu verstehen?

Hier ist gemeint, dass mehrere ähnlich fragmentierte Datensätze kombiniert das gesamte Bundesgebiet abdecken müssten. Ergibt sich in der Zusammenlegung eine Abdeckung des gesamten Bundesgebiets, ist auch die Bereitstellung eines Teil-Datensatzes erforderlich.

<hr>

Um die Änderungen auf dieser Seite besser nachvollziehbar zu machen, bieten wir Versionsvergleiche an:

| Version vom | Kommentar |
| ----------- | --------- |
|[Version 1.1 vom 18.03.2024](https://gitlab.opencode.de/fitko/govdata/govdataportal-hvd-faq/-/commit/e73fdb7b9198e5c7b8d9915da8290c1cc41b7344)  |Umfangreiche Erweiterung der Fragen und Antworten _(Bitte auf "This diff is collapsed. Click to expand it" klicken)_  |
|[Update vom 21.02.2024](https://gitlab.opencode.de/fitko/govdata/govdataportal-hvd-faq/-/commit/39b16463644827fdcf5afe1644408a3c50527cd6)  |Korrigiert das Metadaten-Beispiel  |
|[Version 1.0 vom 21.02.2024](https://gitlab.opencode.de/fitko/govdata/govdataportal-hvd-faq/-/commit/cbe88dec37165e41ed6a9154ceb148002efcbc18)  |Die ersten Fragen und Antworten wurden hinzugefügt. _(Bitte auf "This diff is collapsed. Click to expand it" klicken)_  |
|[Update vom 20.12.2023](https://gitlab.opencode.de/fitko/govdata/govdataportal-hvd-faq/-/commit/ef88bcc633a843c02f4a4bea90fc18c3df688016)  |Der Standard DCAT-AP HVD wurde verabschiedet  |
|[Update vom 23.10.2023](https://gitlab.opencode.de/fitko/govdata/govdataportal-hvd-faq/-/commit/b2fd131d67ccfa0c87a14f4de44e5837163e58ed)  |Ein Metadaten-Beispiel für HVDs wurde hinzugefügt  |
|[10.10.2023](https://gitlab.opencode.de/fitko/govdata/govdataportal-hvd-faq/-/commit/83040326cb9f09e05ec64576a5dcec66365e9646)  |Seite wurde erstellt  |
